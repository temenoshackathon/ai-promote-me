//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.FunctionException;

public interface RemovePromotion extends Function<RemovePromotionInput, RemovePromotionResponse> {

    RemovePromotionResponse invoke(Context<HttpRequest<String>> context, RemovePromotionInput input) throws FunctionException;
    
}
