//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;

public class RemovePromotionInput {

    private RemovePromotionParams params;
    
    public RemovePromotionInput(RemovePromotionParams params) {
        this.params = params;
    }
    
    public java.util.Optional<RemovePromotionParams> getParams() {
        return java.util.Optional.ofNullable(params);
    }
}
