//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.view;

import java.io.Serializable;

public class AllPromotions implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.util.List<PromotionBody> promotionBodys;
    
    public java.util.List<PromotionBody> getPromotionBodys() {
        return promotionBodys;
    }
    
    public void setPromotionBody(java.util.List<PromotionBody> promotionBodys) {
        this.promotionBodys = java.util.Collections.unmodifiableList(promotionBodys);
    }
}
