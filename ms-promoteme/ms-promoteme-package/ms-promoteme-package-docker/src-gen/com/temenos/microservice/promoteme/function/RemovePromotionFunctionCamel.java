//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;
import org.apache.camel.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.function.camel.*;

public class RemovePromotionFunctionCamel implements Processor {

    public void process(Exchange exchange) {
        HttpRequestTransformer<String> httpRequestTransformer = new CamelHttpRequestTransformer("removePromotion", exchange);        
        HttpRequest<String> httpRequest = httpRequestTransformer.transform();

        RemovePromotionParams params = FunctionInputBuilder.buildParams(httpRequest, RemovePromotionParams.class);
        RemovePromotionInput input = new RemovePromotionInput(params);
        HttpRequestContext context = FunctionInputBuilder.buildContext(httpRequest);

        Response<RemovePromotionResponse> output = new RemovePromotionFunction().invoke(context, input);
        
        HttpResponseBuilder<Exchange> httpResponseBuilder = new CamelHttpResponseBuilder<RemovePromotionResponse>(
                exchange, output);
        httpResponseBuilder.build();
    }
}
