//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.view;

import java.io.Serializable;

public class UpdatePromotionResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.lang.String status;
    
    public java.lang.String getStatus() {
        return status;
    }         
    
    public void setStatus(java.lang.String status) {
        this.status = status;
    }       
}
