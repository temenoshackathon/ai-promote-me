//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;

public class CreatePromotionInput {

    private PromotionBody body;
    
    public CreatePromotionInput(PromotionBody body) {
        this.body = body;
    }

    public java.util.Optional<PromotionBody> getBody() {
        return java.util.Optional.ofNullable(body);
    }
}
