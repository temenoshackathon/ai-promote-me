//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;
import org.apache.camel.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.function.camel.*;

public class AllPromotionsFunctionCamel implements Processor {

    public void process(Exchange exchange) {
        HttpRequestTransformer<String> httpRequestTransformer = new CamelHttpRequestTransformer("allPromotions", exchange);        
        HttpRequest<String> httpRequest = httpRequestTransformer.transform();

        AllPromotionsParams params = FunctionInputBuilder.buildParams(httpRequest, AllPromotionsParams.class);
        AllPromotionsInput input = new AllPromotionsInput(params);
        HttpRequestContext context = FunctionInputBuilder.buildContext(httpRequest);

        Response<AllPromotions> output = new AllPromotionsFunction().invoke(context, input);
        
        HttpResponseBuilder<Exchange> httpResponseBuilder = new CamelHttpResponseBuilder<AllPromotions>(
                exchange, output);
        httpResponseBuilder.build();
    }
}
