//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.entity;

import com.temenos.microservice.framework.core.data.Entity;
import com.temenos.microservice.framework.core.data.BaseEntity;
import com.datastax.driver.mapping.annotations.*;
import com.temenos.microservice.framework.core.data.annotations.ExpiryFilter;
import com.temenos.microservice.framework.core.data.annotations.DuplicateFilter;
import java.io.Serializable;

@Table(name = "ms_promoteme_ecb")
public class EbContractBalancesEntity extends BaseEntity implements Serializable, Entity {
    private static final long serialVersionUID = 1L;

	   @PartitionKey
	private java.lang.String accountId;
	
	   @ClusteringColumn
	private java.util.Date lastModifiedDateTime;
	
	   @Column
	private java.lang.String customerId;
	
	   @Column
	private java.lang.String workingBalance;
	
	   @Column
	private java.lang.String onlineActualBalance;
	
	@Column
	private String extensionData;
	
	public String getExtensionData() {
		return extensionData;
	}

	public void setExtensionData(String extensionData) {
		this.extensionData = extensionData;
	}
    public java.lang.String getAccountId() {
        return accountId;
    }
    
    public void setAccountId(java.lang.String accountId) {
        	this.accountId = accountId;
    }
    public java.util.Date getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }
    
    public void setLastModifiedDateTime(java.util.Date lastModifiedDateTime) {
        	this.lastModifiedDateTime = lastModifiedDateTime;
    }
    public java.lang.String getCustomerId() {
        return customerId;
    }
    
    public void setCustomerId(java.lang.String customerId) {
        	this.customerId = customerId;
    }
    public java.lang.String getWorkingBalance() {
        return workingBalance;
    }
    
    public void setWorkingBalance(java.lang.String workingBalance) {
        	this.workingBalance = workingBalance;
    }
    public java.lang.String getOnlineActualBalance() {
        return onlineActualBalance;
    }
    
    public void setOnlineActualBalance(java.lang.String onlineActualBalance) {
        	this.onlineActualBalance = onlineActualBalance;
    }
	 
}
