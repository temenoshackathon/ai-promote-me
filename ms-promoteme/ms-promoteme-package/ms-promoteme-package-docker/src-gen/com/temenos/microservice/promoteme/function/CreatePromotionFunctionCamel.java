//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;
import org.apache.camel.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.function.camel.*;

public class CreatePromotionFunctionCamel implements Processor {

    public void process(Exchange exchange) {
        HttpRequestTransformer<String> httpRequestTransformer = new CamelHttpRequestTransformer("createPromotion", exchange);        
        HttpRequest<String> httpRequest = httpRequestTransformer.transform();

        PromotionBody body = FunctionInputBuilder.buildBody(httpRequest, PromotionBody.class);
        CreatePromotionInput input = new CreatePromotionInput(body);
        HttpRequestContext context = FunctionInputBuilder.buildContext(httpRequest);

        Response<PromotionBody> output = new CreatePromotionFunction().invoke(context, input);
        
        HttpResponseBuilder<Exchange> httpResponseBuilder = new CamelHttpResponseBuilder<PromotionBody>(
                exchange, output);
        httpResponseBuilder.build();
    }
}
