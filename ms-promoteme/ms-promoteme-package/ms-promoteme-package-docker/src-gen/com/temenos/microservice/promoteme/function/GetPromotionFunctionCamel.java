//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;
import org.apache.camel.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.function.camel.*;

public class GetPromotionFunctionCamel implements Processor {

    public void process(Exchange exchange) {
        HttpRequestTransformer<String> httpRequestTransformer = new CamelHttpRequestTransformer("getPromotion", exchange);        
        HttpRequest<String> httpRequest = httpRequestTransformer.transform();

        GetPromotionParams params = FunctionInputBuilder.buildParams(httpRequest, GetPromotionParams.class);
        GetPromotionInput input = new GetPromotionInput(params);
        HttpRequestContext context = FunctionInputBuilder.buildContext(httpRequest);

        Response<PromotionBody> output = new GetPromotionFunction().invoke(context, input);
        
        HttpResponseBuilder<Exchange> httpResponseBuilder = new CamelHttpResponseBuilder<PromotionBody>(
                exchange, output);
        httpResponseBuilder.build();
    }
}
