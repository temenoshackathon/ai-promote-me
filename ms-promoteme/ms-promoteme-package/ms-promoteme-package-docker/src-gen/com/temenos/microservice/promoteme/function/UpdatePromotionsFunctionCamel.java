//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;
import org.apache.camel.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.function.camel.*;

public class UpdatePromotionsFunctionCamel implements Processor {

    public void process(Exchange exchange) {
        HttpRequestTransformer<String> httpRequestTransformer = new CamelHttpRequestTransformer("updatePromotions", exchange);        
        HttpRequest<String> httpRequest = httpRequestTransformer.transform();

        UpdatePromotionsParams params = FunctionInputBuilder.buildParams(httpRequest, UpdatePromotionsParams.class);
        PromotionBody body = FunctionInputBuilder.buildBody(httpRequest, PromotionBody.class);
        UpdatePromotionsInput input = new UpdatePromotionsInput(params, body);
        HttpRequestContext context = FunctionInputBuilder.buildContext(httpRequest);

        Response<UpdatePromotionResponse> output = new UpdatePromotionsFunction().invoke(context, input);
        
        HttpResponseBuilder<Exchange> httpResponseBuilder = new CamelHttpResponseBuilder<UpdatePromotionResponse>(
                exchange, output);
        httpResponseBuilder.build();
    }
}
