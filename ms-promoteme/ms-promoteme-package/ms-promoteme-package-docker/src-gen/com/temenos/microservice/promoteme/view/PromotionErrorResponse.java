//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.view;

import java.io.Serializable;

public class PromotionErrorResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.util.List<StandardErrorResponse> errorLists;
    
    public java.util.List<StandardErrorResponse> getErrorLists() {
        return errorLists;
    }
    
    public void setErrorList(java.util.List<StandardErrorResponse> errorLists) {
        this.errorLists = java.util.Collections.unmodifiableList(errorLists);
    }
}
