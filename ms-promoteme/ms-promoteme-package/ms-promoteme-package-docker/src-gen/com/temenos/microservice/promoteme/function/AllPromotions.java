//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.FunctionException;

public interface AllPromotions extends Function<AllPromotionsInput, AllPromotions> {

    AllPromotions invoke(Context<HttpRequest<String>> context, AllPromotionsInput input) throws FunctionException;
    
}
