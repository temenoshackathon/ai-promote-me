//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;

public class GetPromotionInput {

    private GetPromotionParams params;
    
    public GetPromotionInput(GetPromotionParams params) {
        this.params = params;
    }
    
    public java.util.Optional<GetPromotionParams> getParams() {
        return java.util.Optional.ofNullable(params);
    }
}
