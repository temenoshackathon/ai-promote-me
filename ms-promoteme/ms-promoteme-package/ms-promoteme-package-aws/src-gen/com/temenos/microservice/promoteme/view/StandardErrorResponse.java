//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.view;

import java.io.Serializable;

public class StandardErrorResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.lang.String errorCode;
    
    public java.lang.String getErrorCode() {
        return errorCode;
    }         
    
    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }       
}
