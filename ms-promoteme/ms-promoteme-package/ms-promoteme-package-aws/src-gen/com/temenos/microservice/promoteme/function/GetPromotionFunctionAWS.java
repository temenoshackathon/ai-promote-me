//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;

import com.amazonaws.services.lambda.runtime.events.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.function.aws.*;

public class GetPromotionFunctionAWS {

    public APIGatewayProxyResponseEvent invoke(APIGatewayProxyRequestEvent requestMessage) { 
        HttpRequestTransformer<String> httpRequestTransformer = new AwsHttpRequestTransformer("getPromotion", requestMessage);
        HttpRequest<String> httpRequest = httpRequestTransformer.transform();

        GetPromotionParams params = FunctionInputBuilder.buildParams(httpRequest, GetPromotionParams.class);
        GetPromotionInput input = new GetPromotionInput(params);
        HttpRequestContext context = FunctionInputBuilder.buildContext(httpRequest);
        
        Response<PromotionBody> output = new GetPromotionFunction().invoke(context, input);
        
        HttpResponseBuilder<APIGatewayProxyResponseEvent> httpResponseBuilder = new AwsHttpResponseBuilder<PromotionBody>(
                output);
        return httpResponseBuilder.build();
    }
}
