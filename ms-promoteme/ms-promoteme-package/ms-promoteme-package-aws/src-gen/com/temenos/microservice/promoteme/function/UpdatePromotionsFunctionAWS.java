//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;

import com.amazonaws.services.lambda.runtime.events.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.function.aws.*;

public class UpdatePromotionsFunctionAWS {

    public APIGatewayProxyResponseEvent invoke(APIGatewayProxyRequestEvent requestMessage) { 
        HttpRequestTransformer<String> httpRequestTransformer = new AwsHttpRequestTransformer("updatePromotions", requestMessage);
        HttpRequest<String> httpRequest = httpRequestTransformer.transform();

        UpdatePromotionsParams params = FunctionInputBuilder.buildParams(httpRequest, UpdatePromotionsParams.class);
        PromotionBody body = FunctionInputBuilder.buildBody(httpRequest, PromotionBody.class);
        UpdatePromotionsInput input = new UpdatePromotionsInput(params, body);
        HttpRequestContext context = FunctionInputBuilder.buildContext(httpRequest);
        
        Response<UpdatePromotionResponse> output = new UpdatePromotionsFunction().invoke(context, input);
        
        HttpResponseBuilder<APIGatewayProxyResponseEvent> httpResponseBuilder = new AwsHttpResponseBuilder<UpdatePromotionResponse>(
                output);
        return httpResponseBuilder.build();
    }
}
