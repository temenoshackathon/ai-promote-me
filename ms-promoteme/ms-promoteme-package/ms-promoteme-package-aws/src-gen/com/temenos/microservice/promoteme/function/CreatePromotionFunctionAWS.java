//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;

import com.amazonaws.services.lambda.runtime.events.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.function.aws.*;

public class CreatePromotionFunctionAWS {

    public APIGatewayProxyResponseEvent invoke(APIGatewayProxyRequestEvent requestMessage) { 
        HttpRequestTransformer<String> httpRequestTransformer = new AwsHttpRequestTransformer("createPromotion", requestMessage);
        HttpRequest<String> httpRequest = httpRequestTransformer.transform();

        PromotionBody body = FunctionInputBuilder.buildBody(httpRequest, PromotionBody.class);
        CreatePromotionInput input = new CreatePromotionInput(body);
        HttpRequestContext context = FunctionInputBuilder.buildContext(httpRequest);
        
        Response<PromotionBody> output = new CreatePromotionFunction().invoke(context, input);
        
        HttpResponseBuilder<APIGatewayProxyResponseEvent> httpResponseBuilder = new AwsHttpResponseBuilder<PromotionBody>(
                output);
        return httpResponseBuilder.build();
    }
}
