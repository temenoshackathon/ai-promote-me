//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.entity;

import com.temenos.microservice.framework.core.data.Entity;
import com.temenos.microservice.framework.core.data.BaseEntity;
import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.temenos.microservice.framework.core.data.annotations.*;
import java.io.Serializable;

@DynamoDBTable(tableName = "ms_promoteme_promotions")
public class PromotionsEntity extends BaseEntity implements Serializable, Entity {
    private static final long serialVersionUID = 1L;

       @DynamoDBHashKey
    private java.lang.String promotionId;
       @DynamoDBAttribute
    private java.lang.String message;
	@DynamoDBAttribute
	private String extensionData;
	
	public String getExtensionData() {
		return extensionData;
	}

	public void setExtensionData(String extensionData) {
		this.extensionData = extensionData;
	}
	
	@DynamoDBVersionAttribute
	private Long version;
	
	public Long getVersion() {
	 return version; 
	}
	
    public void setVersion(Long version) {
     this.version = version;
    }

    public java.lang.String getPromotionId() {
        return promotionId;
    }
    
    public void setPromotionId(java.lang.String promotionId) {
        	this.promotionId = promotionId;
    }
    public java.lang.String getMessage() {
        return message;
    }
    
    public void setMessage(java.lang.String message) {
        	this.message = message;
    }

}
