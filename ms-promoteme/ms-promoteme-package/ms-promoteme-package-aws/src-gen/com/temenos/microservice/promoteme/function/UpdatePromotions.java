//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.FunctionException;

public interface UpdatePromotions extends Function<UpdatePromotionsInput, UpdatePromotionResponse> {

    UpdatePromotionResponse invoke(Context<HttpRequest<String>> context, UpdatePromotionsInput input) throws FunctionException;
    
}
