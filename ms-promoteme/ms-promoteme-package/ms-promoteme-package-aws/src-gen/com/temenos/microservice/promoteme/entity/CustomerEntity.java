//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.entity;

import com.temenos.microservice.framework.core.data.Entity;
import com.temenos.microservice.framework.core.data.BaseEntity;
import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.temenos.microservice.framework.core.data.annotations.*;
import java.io.Serializable;

@DynamoDBTable(tableName = "ms_promoteme_customer")
public class CustomerEntity extends BaseEntity implements Serializable, Entity {
    private static final long serialVersionUID = 1L;

       @DynamoDBHashKey
    private java.lang.String customerId;
       @DynamoDBAttribute
    private java.lang.String firstName;
       @DynamoDBAttribute
    private java.lang.String gender;
       @DynamoDBAttribute
    private java.lang.String maritalStatus;
       @DynamoDBAttribute
    private java.util.Date dateOfBirth;
       @DynamoDBAttribute
    private java.lang.String language;
       @DynamoDBAttribute
    private java.lang.String activeStatus;
       @DynamoDBAttribute
    private java.lang.String promoteMe;
	@DynamoDBAttribute
	private String extensionData;
	
	public String getExtensionData() {
		return extensionData;
	}

	public void setExtensionData(String extensionData) {
		this.extensionData = extensionData;
	}
	
	@DynamoDBVersionAttribute
	private Long version;
	
	public Long getVersion() {
	 return version; 
	}
	
    public void setVersion(Long version) {
     this.version = version;
    }

    public java.lang.String getCustomerId() {
        return customerId;
    }
    
    public void setCustomerId(java.lang.String customerId) {
        	this.customerId = customerId;
    }
    public java.lang.String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(java.lang.String firstName) {
        	this.firstName = firstName;
    }
    public java.lang.String getGender() {
        return gender;
    }
    
    public void setGender(java.lang.String gender) {
        	this.gender = gender;
    }
    public java.lang.String getMaritalStatus() {
        return maritalStatus;
    }
    
    public void setMaritalStatus(java.lang.String maritalStatus) {
        	this.maritalStatus = maritalStatus;
    }
    public java.util.Date getDateOfBirth() {
        return dateOfBirth;
    }
    
    public void setDateOfBirth(java.util.Date dateOfBirth) {
        	this.dateOfBirth = dateOfBirth;
    }
    public java.lang.String getLanguage() {
        return language;
    }
    
    public void setLanguage(java.lang.String language) {
        	this.language = language;
    }
    public java.lang.String getActiveStatus() {
        return activeStatus;
    }
    
    public void setActiveStatus(java.lang.String activeStatus) {
        	this.activeStatus = activeStatus;
    }
    public java.lang.String getPromoteMe() {
        return promoteMe;
    }
    
    public void setPromoteMe(java.lang.String promoteMe) {
        	this.promoteMe = promoteMe;
    }

}
