//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;

import com.amazonaws.services.lambda.runtime.events.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.function.aws.*;

public class AllPromotionsFunctionAWS {

    public APIGatewayProxyResponseEvent invoke(APIGatewayProxyRequestEvent requestMessage) { 
        HttpRequestTransformer<String> httpRequestTransformer = new AwsHttpRequestTransformer("allPromotions", requestMessage);
        HttpRequest<String> httpRequest = httpRequestTransformer.transform();

        AllPromotionsParams params = FunctionInputBuilder.buildParams(httpRequest, AllPromotionsParams.class);
        AllPromotionsInput input = new AllPromotionsInput(params);
        HttpRequestContext context = FunctionInputBuilder.buildContext(httpRequest);
        
        Response<AllPromotions> output = new AllPromotionsFunction().invoke(context, input);
        
        HttpResponseBuilder<APIGatewayProxyResponseEvent> httpResponseBuilder = new AwsHttpResponseBuilder<AllPromotions>(
                output);
        return httpResponseBuilder.build();
    }
}
