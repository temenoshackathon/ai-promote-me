//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.entity;

import com.temenos.microservice.framework.core.data.Entity;
import com.temenos.microservice.framework.core.data.BaseEntity;
import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.temenos.microservice.framework.core.data.annotations.*;
import java.io.Serializable;

@DynamoDBTable(tableName = "ms_promoteme_ecb")
public class EbContractBalancesEntity extends BaseEntity implements Serializable, Entity {
    private static final long serialVersionUID = 1L;

       @DynamoDBHashKey
    private java.lang.String accountId;
       @DynamoDBRangeKey
    private java.util.Date lastModifiedDateTime;
       @DynamoDBAttribute
    private java.lang.String customerId;
       @DynamoDBAttribute
    private java.lang.String workingBalance;
       @DynamoDBAttribute
    private java.lang.String onlineActualBalance;
	@DynamoDBAttribute
	private String extensionData;
	
	public String getExtensionData() {
		return extensionData;
	}

	public void setExtensionData(String extensionData) {
		this.extensionData = extensionData;
	}
	
	@DynamoDBVersionAttribute
	private Long version;
	
	public Long getVersion() {
	 return version; 
	}
	
    public void setVersion(Long version) {
     this.version = version;
    }

    public java.lang.String getAccountId() {
        return accountId;
    }
    
    public void setAccountId(java.lang.String accountId) {
        	this.accountId = accountId;
    }
    public java.util.Date getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }
    
    public void setLastModifiedDateTime(java.util.Date lastModifiedDateTime) {
        	this.lastModifiedDateTime = lastModifiedDateTime;
    }
    public java.lang.String getCustomerId() {
        return customerId;
    }
    
    public void setCustomerId(java.lang.String customerId) {
        	this.customerId = customerId;
    }
    public java.lang.String getWorkingBalance() {
        return workingBalance;
    }
    
    public void setWorkingBalance(java.lang.String workingBalance) {
        	this.workingBalance = workingBalance;
    }
    public java.lang.String getOnlineActualBalance() {
        return onlineActualBalance;
    }
    
    public void setOnlineActualBalance(java.lang.String onlineActualBalance) {
        	this.onlineActualBalance = onlineActualBalance;
    }

}
