//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;

import com.amazonaws.services.lambda.runtime.events.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.function.aws.*;

public class RemovePromotionFunctionAWS {

    public APIGatewayProxyResponseEvent invoke(APIGatewayProxyRequestEvent requestMessage) { 
        HttpRequestTransformer<String> httpRequestTransformer = new AwsHttpRequestTransformer("removePromotion", requestMessage);
        HttpRequest<String> httpRequest = httpRequestTransformer.transform();

        RemovePromotionParams params = FunctionInputBuilder.buildParams(httpRequest, RemovePromotionParams.class);
        RemovePromotionInput input = new RemovePromotionInput(params);
        HttpRequestContext context = FunctionInputBuilder.buildContext(httpRequest);
        
        Response<RemovePromotionResponse> output = new RemovePromotionFunction().invoke(context, input);
        
        HttpResponseBuilder<APIGatewayProxyResponseEvent> httpResponseBuilder = new AwsHttpResponseBuilder<RemovePromotionResponse>(
                output);
        return httpResponseBuilder.build();
    }
}
