#!/bin/bash -x

# Setup the environment
export PATH=/opt/apache-maven-3.3.9/bin:/usr/bin:/usr/sbin:/usr/local/bin:$PATH
export MAVEN_OPTS="-Xmx2048m"

# Delete Dynamo DB Tables
aws dynamodb delete-table --table-name ms_promoteme_customer
sleep 10
aws dynamodb delete-table --table-name ms_promoteme_ecb
sleep 10
aws dynamodb delete-table --table-name ms_promoteme_promotions
sleep 10



# Delete Kinesis Stream
aws kinesis delete-stream --stream-name table-update-promoteme
aws kinesis delete-stream --stream-name error-promoteme

# Delete Ingester
export eventSourceMappingUUID=$(aws lambda list-event-source-mappings | python -c 'import json,sys;mappings=json.load(sys.stdin); filter=[mapping for mapping in mappings["EventSourceMappings"] if "function:promoteme-ingester" in mapping["FunctionArn"]]; print filter[0]["UUID"]')
aws lambda delete-event-source-mapping --uuid $eventSourceMappingUUID
aws lambda delete-function --function-name promoteme-ingester

# Delete holdings API functions
aws lambda delete-function --function-name promoteme-createPromotion
sleep 10
aws lambda delete-function --function-name promoteme-getPromotion
sleep 10
aws lambda delete-function --function-name promoteme-updatePromotions
sleep 10
aws lambda delete-function --function-name promoteme-removePromotion
sleep 10
aws lambda delete-function --function-name promoteme-allPromotions
sleep 10

# Delete REST API
export restAPIId=$(aws apigateway get-rest-apis | python -c 'import json,sys;apis=json.load(sys.stdin); filter=[api for api in apis["items"] if "ms-promoteme-api" == api["name"]]; print filter[0]["id"]')
aws apigateway delete-rest-api --rest-api-id $restAPIId

# Delete API Key
export apiKeyId=$(aws apigateway get-api-keys | python -c 'import json,sys;keys=json.load(sys.stdin); filter=[key for key in keys["items"] if "ms-promoteme-apikey" == key["name"]]; print filter[0]["id"]')
aws apigateway delete-api-key --api-key $apiKeyId

# Create usage plan and get Id
export usagePlanId=$(aws apigateway get-usage-plans | python -c 'import json,sys; usagePlans=json.load(sys.stdin); filter=[plan for plan in usagePlans["items"] if "ms-promoteme-usageplan" == plan["name"]]; print filter[0]["id"]')
aws apigateway delete-usage-plan --usage-plan-id $usagePlanId