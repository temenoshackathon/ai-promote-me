#!/bin/bash -x

# Setup the environment
SERVICE_NAME=promoteme
export PATH=/opt/apache-maven-3.3.9/bin:/usr/bin:/usr/sbin:/usr/local/bin:$PATH
export MAVEN_OPTS="-Xmx2048m"

# Create Dynamo DB Tables
aws dynamodb create-table --table-name ms_promoteme_customer --attribute-definitions AttributeName=customerId,AttributeType=S  --key-schema AttributeName=customerId,KeyType=HASH  --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=1
sleep 10
aws dynamodb create-table --table-name ms_promoteme_ecb --attribute-definitions AttributeName=accountId,AttributeType=S AttributeName=lastModifiedDateTime,AttributeType=S  --key-schema AttributeName=accountId,KeyType=HASH AttributeName=lastModifiedDateTime,KeyType=RANGE  --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=1
sleep 10
aws dynamodb create-table --table-name ms_promoteme_promotions --attribute-definitions AttributeName=promotionId,AttributeType=S  --key-schema AttributeName=promotionId,KeyType=HASH  --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=1
sleep 10


# Create Kinesis Stream
aws kinesis create-stream --stream-name table-update-promoteme --shard-count 1
sleep 10
aws kinesis create-stream --stream-name error-promoteme --shard-count 1
sleep 10

# Before installing functions, get the fileName of the jar from /app
export serviceFileName=$(ls app)


# Install Ingester
aws lambda create-function --function-name promoteme-ingester --runtime java8 --role arn:aws:iam::177642146375:role/lambda-kinesis-execution-role --handler com.temenos.microservice.framework.ingester.instance.KinesisEventProcessor::handleRequest --description "Ingester for Micro Service" --timeout 120 --memory-size 256 --publish --tags FunctionType=Ingester,Service=promoteme --zip-file fileb://app/${serviceFileName} --environment Variables=\{\}
sleep 10
aws lambda create-event-source-mapping --event-source-arn arn:aws:kinesis:eu-west-2:177642146375:stream/table-update-promoteme --function-name promoteme-ingester --enabled --batch-size 100 --starting-position LATEST
sleep 10


#Create lambda Functions
aws lambda create-function --function-name promoteme-createPromotion --runtime java8 --role arn:aws:iam::177642146375:role/lambda_basic_execution --handler com.temenos.microservice.promoteme.function.CreatePromotionFunctionAWS::invoke --description "An Api Function for createPromotion" --timeout 120 --memory-size 256 --publish --tags FunctionType=API,Service=promoteme --zip-file fileb://app/${serviceFileName} --environment Variables=\{,className_createPromotion=com.temenos.microservice.promoteme.function.CreatePromotionImpl\}
sleep 10
aws lambda create-function --function-name promoteme-getPromotion --runtime java8 --role arn:aws:iam::177642146375:role/lambda_basic_execution --handler com.temenos.microservice.promoteme.function.GetPromotionFunctionAWS::invoke --description "An Api Function for getPromotion" --timeout 120 --memory-size 256 --publish --tags FunctionType=API,Service=promoteme --zip-file fileb://app/${serviceFileName} --environment Variables=\{,className_getPromotion=com.temenos.microservice.promoteme.function.GetPromotionImpl\}
sleep 10
aws lambda create-function --function-name promoteme-updatePromotions --runtime java8 --role arn:aws:iam::177642146375:role/lambda_basic_execution --handler com.temenos.microservice.promoteme.function.UpdatePromotionsFunctionAWS::invoke --description "An Api Function for updatePromotions" --timeout 120 --memory-size 256 --publish --tags FunctionType=API,Service=promoteme --zip-file fileb://app/${serviceFileName} --environment Variables=\{,className_updatePromotions=com.temenos.microservice.promoteme.function.UpdatePromotionsImpl\}
sleep 10
aws lambda create-function --function-name promoteme-removePromotion --runtime java8 --role arn:aws:iam::177642146375:role/lambda_basic_execution --handler com.temenos.microservice.promoteme.function.RemovePromotionFunctionAWS::invoke --description "An Api Function for removePromotion" --timeout 120 --memory-size 256 --publish --tags FunctionType=API,Service=promoteme --zip-file fileb://app/${serviceFileName} --environment Variables=\{,className_removePromotion=com.temenos.microservice.promoteme.function.RemovePromotionImpl\}
sleep 10
aws lambda create-function --function-name promoteme-allPromotions --runtime java8 --role arn:aws:iam::177642146375:role/lambda_basic_execution --handler com.temenos.microservice.promoteme.function.AllPromotionsFunctionAWS::invoke --description "An Api Function for allPromotions" --timeout 120 --memory-size 256 --publish --tags FunctionType=API,Service=promoteme --zip-file fileb://app/${serviceFileName} --environment Variables=\{,className_allPromotions=com.temenos.microservice.promoteme.function.AllPromotionsImpl\}
sleep 10


# Create REST API
export restAPIId=$(aws apigateway create-rest-api --name ms-promoteme-api --description "Resource For API" | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["id"]')

aws apigateway put-gateway-response --rest-api-id $restAPIId --response-type MISSING_AUTHENTICATION_TOKEN --status-code 404

# Get root resource id for all other resources
export apiRootResourcetId=$(aws apigateway get-resources --rest-api-id $restAPIId | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["items"][-1]["id"]')

# Create version resource and get id
export versionResourceId=$(aws apigateway create-resource --rest-api-id $restAPIId --parent-id $apiRootResourcetId --path-part "v1.0.0" | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["id"]')

# Api Gateway Functions
export promotionsResourceId=$(aws apigateway create-resource --rest-api-id $restAPIId --parent-id $versionResourceId --path-part "promotions" | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["id"]')

aws apigateway put-method --rest-api-id $restAPIId --resource-id $null --http-method POST --authorization-type NONE --api-key-required --region eu-west-2 
aws apigateway put-integration --rest-api-id $restAPIId --resource-id $null --http-method POST --type AWS_PROXY --uri arn:aws:apigateway:eu-west-2:lambda:path/2015-03-31/functions/arn:aws:lambda:eu-west-2:177642146375:function:promoteme-createPromotion/invocations --credentials arn:aws:iam::177642146375:role/apigatewayrole --integration-http-method POST --content-handling CONVERT_TO_TEXT

export promotionspromotionIdResourceId=$(aws apigateway create-resource  --rest-api-id $restAPIId  --parent-id $promotionsResourceId --path-part "{promotionId}" | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["id"]')

aws apigateway put-method --rest-api-id $restAPIId --resource-id $promotionspromotionIdResourceId --http-method GET --authorization-type NONE --api-key-required --region eu-west-2 
aws apigateway put-integration --rest-api-id $restAPIId --resource-id $promotionspromotionIdResourceId --http-method GET --type AWS_PROXY --uri arn:aws:apigateway:eu-west-2:lambda:path/2015-03-31/functions/arn:aws:lambda:eu-west-2:177642146375:function:promoteme-getPromotion/invocations --credentials arn:aws:iam::177642146375:role/apigatewayrole --integration-http-method POST --content-handling CONVERT_TO_TEXT

aws apigateway put-method --rest-api-id $restAPIId --resource-id $promotionspromotionIdResourceId --http-method PUT --authorization-type NONE --api-key-required --region eu-west-2 
aws apigateway put-integration --rest-api-id $restAPIId --resource-id $promotionspromotionIdResourceId --http-method PUT --type AWS_PROXY --uri arn:aws:apigateway:eu-west-2:lambda:path/2015-03-31/functions/arn:aws:lambda:eu-west-2:177642146375:function:promoteme-updatePromotions/invocations --credentials arn:aws:iam::177642146375:role/apigatewayrole --integration-http-method POST --content-handling CONVERT_TO_TEXT

aws apigateway put-method --rest-api-id $restAPIId --resource-id $promotionspromotionIdResourceId --http-method DELETE --authorization-type NONE --api-key-required --region eu-west-2 
aws apigateway put-integration --rest-api-id $restAPIId --resource-id $promotionspromotionIdResourceId --http-method DELETE --type AWS_PROXY --uri arn:aws:apigateway:eu-west-2:lambda:path/2015-03-31/functions/arn:aws:lambda:eu-west-2:177642146375:function:promoteme-removePromotion/invocations --credentials arn:aws:iam::177642146375:role/apigatewayrole --integration-http-method POST --content-handling CONVERT_TO_TEXT

export allpromotionsResourceId=$(aws apigateway create-resource --rest-api-id $restAPIId --parent-id $versionResourceId --path-part "allpromotions" | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["id"]')

export allpromotionscustomerIdResourceId=$(aws apigateway create-resource  --rest-api-id $restAPIId  --parent-id $allpromotionsResourceId --path-part "{customerId}" | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["id"]')

aws apigateway put-method --rest-api-id $restAPIId --resource-id $allpromotionscustomerIdResourceId --http-method GET --authorization-type NONE --api-key-required --region eu-west-2 
aws apigateway put-integration --rest-api-id $restAPIId --resource-id $allpromotionscustomerIdResourceId --http-method GET --type AWS_PROXY --uri arn:aws:apigateway:eu-west-2:lambda:path/2015-03-31/functions/arn:aws:lambda:eu-west-2:177642146375:function:promoteme-allPromotions/invocations --credentials arn:aws:iam::177642146375:role/apigatewayrole --integration-http-method POST --content-handling CONVERT_TO_TEXT

 

# To publish API so that we can access, create deployment and get id
export deploymentId=$(aws apigateway create-deployment --rest-api-id $restAPIId --stage-name test-primary --stage-description "Function Test Primary" | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["id"]')

# Create API Key
export apiKeyId=$(aws apigateway create-api-key --name ms-promoteme-apikey --description "promoteme api key" --enabled | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["id"]')
export apiKeyValue=$(aws apigateway get-api-key --api-key $apiKeyId --include-value | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["value"]')
export API_KEY=$apiKeyValue

# Create usage plan and get Id
export usagePlanId=$(aws apigateway create-usage-plan --name ms-promoteme-usageplan --api-stages apiId=$restAPIId,stage=test-primary | python -c 'import json,sys;obj=json.load(sys.stdin);print obj["id"]')
# Now map the usage plan with the key
aws apigateway create-usage-plan-key --usage-plan-id $usagePlanId --key-id $apiKeyId --key-type API_KEY

# Finally set the URL for integration test to invoke
export API_BASE_URL=https://${restAPIId}.execute-api.eu-west-2.amazonaws.com/test-primary