//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;

public class AllPromotionsInput {

    private AllPromotionsParams params;
    
    public AllPromotionsInput(AllPromotionsParams params) {
        this.params = params;
    }
    
    public java.util.Optional<AllPromotionsParams> getParams() {
        return java.util.Optional.ofNullable(params);
    }
}
