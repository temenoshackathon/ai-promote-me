//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;

public class UpdatePromotionsInput {

    private UpdatePromotionsParams params;
    private PromotionBody body;
    
    public UpdatePromotionsInput(UpdatePromotionsParams params, PromotionBody body) {
        this.params = params;
        this.body = body;
    }
    
    public java.util.Optional<UpdatePromotionsParams> getParams() {
        return java.util.Optional.ofNullable(params);
    }
    
    public java.util.Optional<PromotionBody> getBody() {
        return java.util.Optional.ofNullable(body);
    }
}
