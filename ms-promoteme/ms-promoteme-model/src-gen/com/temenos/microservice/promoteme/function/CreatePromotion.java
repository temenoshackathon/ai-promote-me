//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.FunctionException;

public interface CreatePromotion extends Function<CreatePromotionInput, PromotionBody> {

    PromotionBody invoke(Context<HttpRequest<String>> context, CreatePromotionInput input) throws FunctionException;
    
}
