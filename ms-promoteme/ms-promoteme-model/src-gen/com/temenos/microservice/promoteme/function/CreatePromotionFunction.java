//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.promoteme.view.*;
import com.temenos.microservice.framework.core.function.*;
import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.Interceptors;
import com.temenos.logger.Logger;
import com.temenos.logger.event.Event;


public class CreatePromotionFunction {

    private static final Event event = Logger.forEvent().forComp("API");  

    private static int successStatus = 200; 
    private FunctionInvoker<CreatePromotionInput, PromotionBody> functionInvoker = new ConfigBasedFunctionInvoker<CreatePromotionInput, PromotionBody>("createPromotion");
    
    public Response<PromotionBody> invoke(Context<HttpRequest<String>> context, CreatePromotionInput input) {
         HttpRequest httpRequest = (HttpRequest) context.getRequest();
        try {
           event.prepareInfo().tag("HttpRequestEntry","http request has entered into microservice").tag("Request UUID",httpRequest.getUUID())
                           .tag("Operation Id",httpRequest.getOperationId())
				           .log();
            Interceptors.authzApiInterceptor().intercept(context.getRequest().getAuthzContext());    
            PromotionBody output = functionInvoker.invoke(context, input);
            return Response.newSuccess(successStatus, output);
        } catch (FunctionException e) {
            return Response.newFailure(e);
        }finally{
         event.prepareInfo().tag("HttpRequestExit","http request has exitted into microservice").tag("Request UUID",httpRequest.getUUID())
                           .tag("Operation Id",httpRequest.getOperationId())
				           .log();
	    }
    }
}
