//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.view;

import java.io.Serializable;

public class PromotionBody implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.lang.String promotionId;
    private java.lang.String message;
    
    public java.lang.String getPromotionId() {
        return promotionId;
    }         
    
    public void setPromotionId(java.lang.String promotionId) {
        this.promotionId = promotionId;
    }       
    public java.lang.String getMessage() {
        return message;
    }         
    
    public void setMessage(java.lang.String message) {
        this.message = message;
    }       
}
