//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.view;

import java.io.Serializable;

public class RemovePromotionParams implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.util.List<java.lang.String> promotionIds;
    
    public java.util.List<java.lang.String> getPromotionIds() {
        return promotionIds;
    }
    
    public void setPromotionId(java.util.List<java.lang.String> promotionIds) {
        this.promotionIds = java.util.Collections.unmodifiableList(promotionIds);
    }
}
