//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.view;

import java.io.Serializable;

public class AllPromotionsParams implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.util.List<java.lang.String> customerIds;
    
    public java.util.List<java.lang.String> getCustomerIds() {
        return customerIds;
    }
    
    public void setCustomerId(java.util.List<java.lang.String> customerIds) {
        this.customerIds = java.util.Collections.unmodifiableList(customerIds);
    }
}
