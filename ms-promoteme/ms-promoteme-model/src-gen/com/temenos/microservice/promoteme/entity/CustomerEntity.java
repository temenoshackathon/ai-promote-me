//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.entity;

import com.temenos.microservice.framework.core.data.Entity;
import com.temenos.microservice.framework.core.data.BaseEntity;
import com.temenos.microservice.framework.core.data.annotations.*;
import java.io.Serializable;

public class CustomerEntity extends BaseEntity implements Serializable, Entity {
    private static final long serialVersionUID = 1L;

    @PartitionKey
	private java.lang.String customerId;
    
	private java.lang.String firstName;
    
	private java.lang.String gender;
    
	private java.lang.String maritalStatus;
    
	private java.util.Date dateOfBirth;
    
	private java.lang.String language;
    
	private java.lang.String activeStatus;
    
	private java.lang.String promoteMe;
    public java.lang.String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(java.lang.String customerId) {
        	this.customerId = customerId;
    }
    public java.lang.String getFirstName() {
        return firstName;
    }

    public void setFirstName(java.lang.String firstName) {
        	this.firstName = firstName;
    }
    public java.lang.String getGender() {
        return gender;
    }

    public void setGender(java.lang.String gender) {
        	this.gender = gender;
    }
    public java.lang.String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(java.lang.String maritalStatus) {
        	this.maritalStatus = maritalStatus;
    }
    public java.util.Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(java.util.Date dateOfBirth) {
        	this.dateOfBirth = dateOfBirth;
    }
    public java.lang.String getLanguage() {
        return language;
    }

    public void setLanguage(java.lang.String language) {
        	this.language = language;
    }
    public java.lang.String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(java.lang.String activeStatus) {
        	this.activeStatus = activeStatus;
    }
    public java.lang.String getPromoteMe() {
        return promoteMe;
    }

    public void setPromoteMe(java.lang.String promoteMe) {
        	this.promoteMe = promoteMe;
    }
	private String extensionData;
	
	public String getExtensionData() {
		return extensionData;
	}

	public void setExtensionData(String extensionData) {
		this.extensionData = extensionData;
	}
}
