//***AUTO GENERATED CODE. DO NOT EDIT***.
package com.temenos.microservice.promoteme.entity;

import com.temenos.microservice.framework.core.data.Entity;
import com.temenos.microservice.framework.core.data.BaseEntity;
import com.temenos.microservice.framework.core.data.annotations.*;
import java.io.Serializable;

public class PromotionsEntity extends BaseEntity implements Serializable, Entity {
    private static final long serialVersionUID = 1L;

    @PartitionKey
	private java.lang.String promotionId;
    
	private java.lang.String message;
    public java.lang.String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(java.lang.String promotionId) {
        	this.promotionId = promotionId;
    }
    public java.lang.String getMessage() {
        return message;
    }

    public void setMessage(java.lang.String message) {
        	this.message = message;
    }
	private String extensionData;
	
	public String getExtensionData() {
		return extensionData;
	}

	public void setExtensionData(String extensionData) {
		this.extensionData = extensionData;
	}
}
