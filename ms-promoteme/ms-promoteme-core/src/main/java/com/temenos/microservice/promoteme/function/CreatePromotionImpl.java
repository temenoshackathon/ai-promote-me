package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.promoteme.view.PromotionBody;

public class CreatePromotionImpl implements CreatePromotion{

	private PromoteProcess promoteProcess;
	
	//Constructor to create instance for  DAO & Transform.
	public CreatePromotionImpl() throws FunctionException {
		promoteProcess = new PromoteProcess();
	}
		
	@Override
	public PromotionBody invoke(Context ctx, CreatePromotionInput input) throws FunctionException {
		PromotionBody promotionBody = input.getBody().get();
		promoteProcess.savePromotion(promotionBody);
		return promotionBody;
	}

		
}
