package com.temenos.microservice.promoteme.dao;

import java.util.List;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.data.DaoFactory;
import com.temenos.microservice.framework.core.data.NoSqlDbDao;
import com.temenos.microservice.promoteme.entity.EbContractBalancesEntity;

public class EbContractBalancesDaoImpl implements EbContractBalancesDao{

	
	private static volatile EbContractBalancesDaoImpl instance;
	private static NoSqlDbDao<EbContractBalancesEntity> dbDao;
	
    private EbContractBalancesDaoImpl() throws FunctionException {
        dbDao = DaoFactory.getDao(EbContractBalancesEntity.class);
    }
    
    public static EbContractBalancesDaoImpl instance() throws FunctionException {

        if (instance == null) {
            synchronized (EbContractBalancesDaoImpl.class) {
                if (instance == null) {
                    instance = new EbContractBalancesDaoImpl();
                }
            }
        }
        return instance;
    }

	
	@Override
	public void updateECB(EbContractBalancesEntity ebContractBalancesEntity) throws FunctionException {
		dbDao.saveEntity(ebContractBalancesEntity);
		
	}

	@Override
	public List<EbContractBalancesEntity> getAllBalance() throws FunctionException {
		return dbDao.get();
	}
	
	

}
