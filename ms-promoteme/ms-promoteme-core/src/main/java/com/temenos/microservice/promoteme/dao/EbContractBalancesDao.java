package com.temenos.microservice.promoteme.dao;

import java.util.List;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.promoteme.entity.EbContractBalancesEntity;

public interface EbContractBalancesDao {

	void updateECB(EbContractBalancesEntity ebContractBalancesEntity) throws FunctionException;
	
	List<EbContractBalancesEntity> getAllBalance() throws FunctionException;
}
