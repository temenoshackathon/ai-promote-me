package com.temenos.microservice.promoteme.dao;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.promoteme.entity.CustomerEntity;

public interface CustomerDao {

	void updateCustomer(CustomerEntity customerEntity) throws FunctionException;
	
	CustomerEntity getCustomerById(String customerId) throws FunctionException;
	
}
