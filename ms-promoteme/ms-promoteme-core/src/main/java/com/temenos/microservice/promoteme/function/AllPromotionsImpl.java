package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.promoteme.function.AllPromotions;
import com.temenos.microservice.promoteme.function.AllPromotionsInput;
import com.temenos.microservice.promoteme.view.Promotions;

public class AllPromotionsImpl implements AllPromotions{

	private PromoteProcess promoteProcess;
	
	//Constructor to create instance for  DAO & Transform.
	public AllPromotionsImpl() throws FunctionException {
		promoteProcess = new PromoteProcess();
	}
		
	@Override
	public Promotions invoke(Context ctx, AllPromotionsInput input) throws FunctionException {
		
		String customerId = input.getParams().get().getCustomerIds().get(0);
		
		return promoteProcess.getAllPromotions(customerId);
	}
	
}
