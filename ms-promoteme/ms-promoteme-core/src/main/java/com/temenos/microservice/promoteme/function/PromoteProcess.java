package com.temenos.microservice.promoteme.function;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.FailureMessage;
import com.temenos.microservice.framework.core.function.FunctionInvocationException;
import com.temenos.microservice.framework.core.function.InvalidInputException;
import com.temenos.microservice.promoteme.dao.CustomerDao;
import com.temenos.microservice.promoteme.dao.CustomerDaoImpl;
import com.temenos.microservice.promoteme.dao.EbContractBalancesDao;
import com.temenos.microservice.promoteme.dao.EbContractBalancesDaoImpl;
import com.temenos.microservice.promoteme.dao.PromotionsDao;
import com.temenos.microservice.promoteme.dao.PromotionsDaoImpl;
import com.temenos.microservice.promoteme.entity.CustomerEntity;
import com.temenos.microservice.promoteme.entity.EbContractBalancesEntity;
import com.temenos.microservice.promoteme.entity.PromotionsEntity;
import com.temenos.microservice.promoteme.view.PromotionBody;
import com.temenos.microservice.promoteme.view.Promotions;

public class PromoteProcess {

	
	private PromotionsDao promotionsDao;
	private CustomerDao  customerDao;
	private EbContractBalancesDao ebContractBalancesDao;
	
	public PromoteProcess() throws FunctionException{
		promotionsDao = createDaoInstance();
		customerDao = CustomerDaoImpl.instance();
		ebContractBalancesDao = EbContractBalancesDaoImpl.instance();
	}
	
	public PromotionsDao createDaoInstance() throws FunctionException {
		try {
			promotionsDao = PromotionsDaoImpl.instance();
		}catch(Exception e) {
			throw new FunctionInvocationException(e);
		}
		return promotionsDao;
	}
	
	public Promotions getAllPromotions(String customerId) throws FunctionException {
		Promotions promotions = new Promotions();
		List<PromotionBody> promotionBodyList = new ArrayList<>();
		PromotionBody promotionBody = new PromotionBody();
		CustomerEntity customerEntity = customerDao.getCustomerById(customerId);
		if(customerEntity.getCustomerId() == null) {
			throw new InvalidInputException(new FailureMessage("Invalid Customer Id","1001"));
		}
		Calendar calendarDOB = Calendar.getInstance();
		calendarDOB.setTime(customerEntity.getDateOfBirth());
		Calendar calendarToday = Calendar.getInstance();
		calendarToday.setTime(new Date());
		int monthDOB = calendarDOB.get(Calendar.MONTH);
		int dayDOB = calendarDOB.get(Calendar.DAY_OF_MONTH);
		int monthCurr = calendarToday.get(Calendar.MONTH);
		int dayToday = calendarToday.get(Calendar.DAY_OF_MONTH);
		calendarDOB.add(Calendar.MONTH,-1);
		int monthLast =  calendarToday.get(Calendar.MONTH);
		if(dayDOB == dayToday && monthDOB == monthCurr) {
			promotionBody.setPromotionId("Birthday");
			promotionBody.setMessage("Wish You Happy Birthday "+customerEntity.getFirstName());
			promotionBodyList.add(promotionBody);
		}
	    List<PromotionsEntity> promotionsEntityList = new ArrayList<>();
		promotionsEntityList = promotionsDao.getAllPromotion();
		for(PromotionsEntity promotionEntity :promotionsEntityList) {
			promotionBody = new PromotionBody();
			promotionBody.setMessage(promotionEntity.getMessage());
			promotionBody.setPromotionId(promotionEntity.getPromotionId());
			promotionBodyList.add(promotionBody);
		}
		List<EbContractBalancesEntity> ebBalancesEntitieList = ebContractBalancesDao.getAllBalance();
		Double totalAmountCurr = 0.0;
		Double totalAmountLast = 0.0;
		for(EbContractBalancesEntity ebContractBalancesEntity :  ebBalancesEntitieList) {
			if(ebContractBalancesEntity.getCustomerId().equals(customerId)) {
				Calendar calendar = Calendar.getInstance();	
				calendar.setTime(ebContractBalancesEntity.getLastModifiedDateTime());
				
				int month= calendar.get(Calendar.MONTH);
				if(month == monthCurr) {
					totalAmountCurr = totalAmountCurr + Double.parseDouble(ebContractBalancesEntity.getWorkingBalance());
				}
				if(month == monthLast) {
					totalAmountLast = totalAmountLast+Double.parseDouble(ebContractBalancesEntity.getWorkingBalance());
				}
			}
		}
		if(totalAmountCurr >= 10000) {
			promotionBody = new PromotionBody();
			promotionBody.setMessage("Seem to be like you are been maintaining more than 1 lakh for past two months. We recommend you to open Fixed Deposits or Recurring Deposits to earn more interest.");
			promotionBody.setPromotionId("FixedDeposit");
			promotionBodyList.add(promotionBody);
		}
		if(totalAmountCurr >= 20000) {
			promotionBody = new PromotionBody();
			promotionBody.setMessage("You are eligible for availing the Loan of 8lakhs.");
			promotionBody.setPromotionId("Loan");
			promotionBodyList.add(promotionBody);
		}
		
		promotions.setPromotionBody(promotionBodyList);
		return promotions;
	}
	
	public void savePromotion(PromotionBody promotionBody) throws FunctionException {
		PromotionsEntity promotionsEntity = new PromotionsEntity();
		PromotionsEntity promotionsEntityCheck = promotionsDao.getPromotion(promotionBody.getPromotionId());
		if(promotionsEntityCheck.getPromotionId() != null) {
			throw new InvalidInputException(new FailureMessage("Promotion available with the Promotion Id."));
		}
		promotionsEntity.setPromotionId(promotionBody.getPromotionId());
		promotionsEntity.setMessage(promotionBody.getMessage());
		promotionsDao.updatePromotion(promotionsEntity);
	} 
	
	public PromotionBody getPromotionById(String promotionId) throws FunctionException {
		PromotionBody response = new PromotionBody();
		PromotionsEntity promotionsEntity = promotionsDao.getPromotion(promotionId);
		response.setMessage(promotionsEntity.getMessage());
		response.setPromotionId(promotionsEntity.getPromotionId());
		return response;
	}
	
	public void removePromotion(String promotionId) throws FunctionException{
		promotionsDao.removePromotion(promotionId);
	}
	
	public void updatePromotion(String promotionId, PromotionBody promotionBody) throws FunctionException {
		PromotionsEntity promotionsEntity = promotionsDao.getPromotion(promotionId);
		promotionsEntity.setPromotionId(promotionBody.getPromotionId());
		promotionsEntity.setMessage(promotionBody.getMessage());
		promotionsDao.updatePromotion(promotionsEntity);
	}
	
}
