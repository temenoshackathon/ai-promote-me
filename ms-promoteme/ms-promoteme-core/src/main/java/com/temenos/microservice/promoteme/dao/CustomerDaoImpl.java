package com.temenos.microservice.promoteme.dao;

import java.util.List;
import java.util.Optional;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.data.DaoFactory;
import com.temenos.microservice.framework.core.data.NoSqlDbDao;
import com.temenos.microservice.promoteme.entity.CustomerEntity;

public class CustomerDaoImpl implements CustomerDao{

	
	private static volatile CustomerDaoImpl instance;
	private static NoSqlDbDao<CustomerEntity> dbDao;
	
    private CustomerDaoImpl() throws FunctionException {
        dbDao = DaoFactory.getDao(CustomerEntity.class);
    }
    
    public static CustomerDaoImpl instance() throws FunctionException {

        if (instance == null) {
            synchronized (CustomerDaoImpl.class) {
                if (instance == null) {
                    instance = new CustomerDaoImpl();
                }
            }
        }
        return instance;
    }

    
	@Override
	public void updateCustomer(CustomerEntity customerEntity) throws FunctionException {
		dbDao.saveEntity(customerEntity);
		
	}
	
	public CustomerEntity getCustomerById(String customerId) throws FunctionException {
		Optional<CustomerEntity> response = dbDao.getByPartitionKey(customerId);
		if(!response.isPresent()) {
			return new CustomerEntity();
		}
		return response.get();
		
	}
	

}
