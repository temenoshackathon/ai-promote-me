package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.promoteme.view.PromotionBody;
import com.temenos.microservice.promoteme.view.UpdatePromotionResponse;


public class UpdatePromotionsImpl implements UpdatePromotions{

	private PromoteProcess promoteProcess;
	
	//Constructor to create instance for  DAO & Transform.
	public UpdatePromotionsImpl() throws FunctionException {
		promoteProcess = new PromoteProcess();
	}
	
	
	@Override
	public UpdatePromotionResponse invoke(Context ctx, UpdatePromotionsInput input) throws FunctionException {
		String promotionId = input.getParams().get().getPromotionIds().get(0);
		PromotionBody promotionBody = input.getBody().get();  
		UpdatePromotionResponse updatePromotionResponse = new UpdatePromotionResponse();
		try {
			promoteProcess.updatePromotion(promotionId,promotionBody);
			updatePromotionResponse.setStatus("Successfully Promotion is updated");
		}catch(Exception e) {
			updatePromotionResponse.setStatus("Removal of promotion is unsuccessfull");
		}
		return updatePromotionResponse;
	}
	

}
