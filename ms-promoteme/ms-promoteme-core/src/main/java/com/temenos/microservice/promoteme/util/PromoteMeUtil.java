package com.temenos.microservice.promoteme.util;

import java.util.Date;

public class PromoteMeUtil {

	/**
	 * This method will return the Date corresponding to the milliSeconds
	 * @param strMilli
	 * @return
	 */
	public static Date getDateFromMilliSeconds(String strMilliSec) {
		Date date = null;
		try {
			long milli = Long.valueOf(strMilliSec);
			date = new Date(milli);
		}catch(Exception e) {
		}
		return date;
	}
}
