package com.temenos.microservice.promoteme.dao;

import java.util.List;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.promoteme.entity.PromotionsEntity;

public interface PromotionsDao {

	void updatePromotion(PromotionsEntity promotionEntity) throws FunctionException;

	List<PromotionsEntity> getAllPromotion() throws FunctionException;
	
	PromotionsEntity getPromotion(String Id) throws FunctionException;
	
	void removePromotion(String id) throws FunctionException;
	
	
}
