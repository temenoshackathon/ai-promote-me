package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.promoteme.view.RemovePromotionResponse;

public class RemovePromotionImpl implements RemovePromotion {

	private PromoteProcess promoteProcess;
	
	//Constructor to create instance for  DAO & Transform.
	public RemovePromotionImpl() throws FunctionException {
		promoteProcess = new PromoteProcess();
	}
	
	
	@Override
	public RemovePromotionResponse invoke(Context ctx, RemovePromotionInput input) throws FunctionException {
		String promotionId = input.getParams().get().getPromotionIds().get(0);
		RemovePromotionResponse removePromotionResponse = new RemovePromotionResponse();
		try {
			promoteProcess.removePromotion(promotionId);
			removePromotionResponse.setStatus("Successfully Promotion is removed");
		}catch(Exception e) {
			removePromotionResponse.setStatus("Removal of promotion is unsuccessfull");
		}
		return removePromotionResponse;
	}

}
