package com.temenos.microservice.promoteme.ingester;

import java.text.ParseException;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

import com.temenos.microservice.framework.core.util.DataTypeConverter;

/*
 * CustomerRecord
 *  @author ssubashchandar
 *
 * Object to store the value from Avro Schema (T24 Event)
 */
public class CustomerRecord {

	 private JSONObject sourceRecord;
	 
	 private String customerId;
	 private java.lang.String firstName;
     private java.lang.String gender;
     private java.lang.String maritalStatus;
     private java.util.Date dateOfBirth;
     private java.lang.String language;
     private java.lang.String activeStatus;
     private java.lang.String promoteMe;
	 
	 /*
     * Instance of object created from ingester class
     * @Inparam JSONObject
     * 
     */
	 public CustomerRecord(JSONObject jsonObject) {
        this.sourceRecord = jsonObject;
        transform();
         
	 }

	 /*
     * Method to get the value from the Avro schema payload
     * Values are stored in local variable and which are accessed through the getter and setter.
     */
    private void transform() {
        JSONObject payload = sourceRecord.getJSONObject("payload");
        JSONArray genericJsonArray;
		JSONObject genericJsonObject;
        this.customerId = payload.optString("recId");
    	genericJsonArray = payload.optJSONArray("ARRAY_Name1");
		genericJsonObject = genericJsonArray.optJSONObject(0); // getting only the first array index (GB Name 1 in T24)
		this.firstName = genericJsonObject != null ? genericJsonObject.optString("Name1") : "";
		this.gender = payload.optString("Gender");
		this.maritalStatus = payload.optString("MaritalStatus");
		this.dateOfBirth = convertDate(payload.optString("DateOfBirth"));
		this.language = payload.optString("Language");
		this.activeStatus = payload.optString("CustomerStatus");
		this.promoteMe = null;
    }
    
    private Date convertDate(String dateText) {
        try {
            return DataTypeConverter.toDate(dateText,"yyyyMMdd");
        } catch (ParseException e) {
            return null;
        }
    }


	public JSONObject getSourceRecord() {
		return sourceRecord;
	}

	public void setSourceRecord(JSONObject sourceRecord) {
		this.sourceRecord = sourceRecord;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public java.lang.String getFirstName() {
		return firstName;
	}

	public void setFirstName(java.lang.String firstName) {
		this.firstName = firstName;
	}

	public java.lang.String getGender() {
		return gender;
	}

	public void setGender(java.lang.String gender) {
		this.gender = gender;
	}

	public java.lang.String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(java.lang.String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public java.util.Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(java.util.Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public java.lang.String getLanguage() {
		return language;
	}

	public void setLanguage(java.lang.String language) {
		this.language = language;
	}

	public java.lang.String getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(java.lang.String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public java.lang.String getPromoteMe() {
		return promoteMe;
	}

	public void setPromoteMe(java.lang.String promoteMe) {
		this.promoteMe = promoteMe;
	}
	
    
}
