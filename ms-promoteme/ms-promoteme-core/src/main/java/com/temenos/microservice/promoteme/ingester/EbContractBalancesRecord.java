package com.temenos.microservice.promoteme.ingester;

import java.util.Date;

import org.json.JSONObject;

import com.temenos.microservice.promoteme.util.PromoteMeUtil;

/*
 * EbContractBalancesRecord
 * @author ssubashchandar
 * 
 * Object to store the value from Avro Schema (T24 Event)
 */
public class EbContractBalancesRecord {
	
	JSONObject sourceRecord;
	String accountId;
	String workingBalance;
	String onlineActualBalance;
	Date lastModifiedTimeStamp;
	String customer;
	
	
	
	 /*
     * Instance of object created from ingester class
     * @Inparam JSONObject
     * 
     */
    public EbContractBalancesRecord(JSONObject jsonObject) {
        this.sourceRecord = jsonObject;
        transform();
       
    }
    
    /*
     * Method to get the value from the Avro schema payload
     * Values are stored in local variable and which are accessed through the getter and setter.
     */
    private void transform() {
        
    	JSONObject payload = sourceRecord.getJSONObject("payload");
      
        this.workingBalance = payload.optString("WorkingBalance");
        this.accountId = payload.optString("recId");
        this.onlineActualBalance = payload.optString("OnlineActualBal");
        this.customer = payload.optString("Customer");
        String strMilliSec = sourceRecord.optString("emittedTime");
        this.lastModifiedTimeStamp = PromoteMeUtil.getDateFromMilliSeconds(strMilliSec);
    }

	public JSONObject getSourceRecord() {
		return sourceRecord;
	}

	public void setSourceRecord(JSONObject sourceRecord) {
		this.sourceRecord = sourceRecord;
	}

	public Date getLastModifiedTimeStamp() {
		return lastModifiedTimeStamp;
	}

	public void setLastModifiedTimeStamp(Date lastModifiedTimeStamp) {
		this.lastModifiedTimeStamp = lastModifiedTimeStamp;
	}


	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getWorkingBalance() {
		return workingBalance;
	}

	public void setWorkingBalance(String workingBalance) {
		this.workingBalance = workingBalance;
	}

	public String getOnlineActualBalance() {
		return onlineActualBalance;
	}

	public void setOnlineActualBalance(String onlineActualBalance) {
		this.onlineActualBalance = onlineActualBalance;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}
	
    
}
