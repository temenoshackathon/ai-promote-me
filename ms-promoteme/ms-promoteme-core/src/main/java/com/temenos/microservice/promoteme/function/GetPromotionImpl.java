package com.temenos.microservice.promoteme.function;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.function.Context;
import com.temenos.microservice.promoteme.view.PromotionBody;

public class GetPromotionImpl implements GetPromotion{

	private PromoteProcess promoteProcess;
	
	//Constructor to create instance for  DAO & Transform.
	public GetPromotionImpl() throws FunctionException {
		promoteProcess = new PromoteProcess();
	}
	
	@Override
	public PromotionBody invoke(Context ctx, GetPromotionInput input) throws FunctionException {
		String promotionId = input.getParams().get().getPromotionIds().get(0);
		return promoteProcess.getPromotionById(promotionId);
	}

}
