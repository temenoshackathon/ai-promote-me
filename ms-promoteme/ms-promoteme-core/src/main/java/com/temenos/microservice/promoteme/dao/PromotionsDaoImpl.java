package com.temenos.microservice.promoteme.dao;

import java.util.List;
import java.util.Optional;

import com.temenos.microservice.framework.core.FunctionException;
import com.temenos.microservice.framework.core.data.DaoFactory;
import com.temenos.microservice.framework.core.data.NoSqlDbDao;
import com.temenos.microservice.promoteme.entity.CustomerEntity;
import com.temenos.microservice.promoteme.entity.PromotionsEntity;

public class PromotionsDaoImpl implements PromotionsDao {

	private static volatile PromotionsDaoImpl instance;
	private static NoSqlDbDao<PromotionsEntity> dbDao;
	
    private PromotionsDaoImpl() throws FunctionException {
        dbDao = DaoFactory.getDao(PromotionsEntity.class);
    }
    
    public static PromotionsDaoImpl instance() throws FunctionException {

        if (instance == null) {
            synchronized (EbContractBalancesDaoImpl.class) {
                if (instance == null) {
                    instance = new PromotionsDaoImpl();
                }
            }
        }
        return instance;
    }

	@Override
	public void updatePromotion(PromotionsEntity promotionEntity) throws FunctionException {
		dbDao.saveEntity(promotionEntity);
	}

	@Override
	public List<PromotionsEntity> getAllPromotion() throws FunctionException {
		return dbDao.get();
	}

	@Override
	public PromotionsEntity getPromotion(String Id) throws FunctionException {
		Optional<PromotionsEntity> response = dbDao.getByPartitionKey(Id);
		if(!response.isPresent()) {
			return new PromotionsEntity();
		}
		return response.get();
	}

	@Override
	public void removePromotion(String id) throws FunctionException {
		PromotionsEntity promotionsEntity = getPromotion(id);
		dbDao.deleteEntity(promotionsEntity);
	}

}
